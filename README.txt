INSTALLATION
------------

 * Download the Searchable Option List jquery plugin (from:
   https://github.com/pbauerochse/searchable-option-list) and add it to the
   libraries folder under the directory name "sol".
 * Enable the Views SOL Filter module
 * Navigate to a new or existing view, and enable the SOL options on any
   eligible filter.


EXAMPLE OVERRIDE OF SOL FUNCTIONALITY
--------------------------------------------------------------

(provide example of how to add images to SOL items)