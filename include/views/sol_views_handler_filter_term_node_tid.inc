<?php

/**
 * @file
 * Definition of example_handler_custom_field
 */

/**
 * Provides a custom views field.
 */
class sol_views_handler_filter_term_node_tid extends views_handler_filter_term_node_tid {

  /**
   * Provide default options for exposed filters.
   *
   * @return array
   */
  function expose_options() {
    parent::expose_options();
    _views_sol_filter_extend_expose_options($this);
  }

  function option_definition() {
    $options = parent::option_definition();
    _views_sol_filter_extend_option_definition($options, $this);
    return $options;
  }

  /**
   * Provide default options for exposed filters.
   */
  function build_group_options() {
    parent::build_group_options();
    _views_sol_filter_extend_build_group_options($this);
  }

  /**
   * @param $form
   * @param $form_state
   */
  function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);
    _views_sol_filter_extend_filter_expose_form($form, $form_state, $this);
  }

}