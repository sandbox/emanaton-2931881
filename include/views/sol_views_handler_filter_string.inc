<?php

/**
 * @file
 * Definition of example_handler_custom_field
 */

/**
 * Provides a custom views field.
 */
class sol_views_handler_filter_string extends views_handler_filter_string {

  /**
   * @return array
   */
  function expose_options() {
    $options = parent::expose_options();
    _views_sol_filter_extend_expose_options($options, $this);
    return $options;
  }

  function option_definition() {
    $options = parent::option_definition();
    _views_sol_filter_extend_option_definition($options, $this);
    return $options;
  }

  /**
   * @param $form
   * @param $form_state
   */
  function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);
    _views_sol_filter_extend_filter_expose_form($form, $form_state, $this);
  }

}