(function ($) {
  /**
   * set up SOL controls
   */
  Drupal.behaviors.views_sol_filter = {
    attach: function (context, settings) {
      if (!Drupal.settings.hasOwnProperty('views_sol_filter')) {
        return;
      }

      Drupal.settings.views_sol_filter['callbacks'] = {};

      $.each(Drupal.settings.views_sol_filter.fields, function(key, vars) {
        var $el = $('#' + vars.id, context);
        if (!$el.length) {
          return true;
        }

        var $wrapper = $el.closest('.views-exposed-widget');
        var id = $wrapper.attr('id');
        Drupal.settings.views_sol_filter.callbacks[id] = {
          'init': $.Callbacks('memory'),
          'open': $.Callbacks('memory'),
          'close': $.Callbacks('memory'),
          'change': $.Callbacks('memory')
        };


        $wrapper.addClass('sol-wrapper');

        if (
          vars.hasOwnProperty('compact')
          && !!vars.compact
        ) {
          $wrapper.addClass('sol-compact');
        }

        if (
          vars.hasOwnProperty('hideSearch')
          && !!vars.hideSearch
        ) {
          $wrapper.addClass('sol-hide-search');
        }

        // for config options, see: http://pbauerochse.github.io/searchable-option-list/configuration.html
        var options =
          $el.prop('multiple')
            ? {
              allowNullSelection: true,
              showSelectAll: true,
              // multiple: true,
              maxHeight: 200
            }
            : {
              maxHeight: 200
            };

        if (vars.hasOwnProperty('texts')) {
          options['texts'] = vars.texts;
        }

        if (vars.hasOwnProperty('showSelectionBelowList')) {
          options['showSelectionBelowList'] = vars.showSelectionBelowList;
        }

        options['events'] = {
          'onInitialized': function(sol, items) {
            $.views_sol_filter_configure_sol(sol, true, true);
            Drupal.settings.views_sol_filter.callbacks[id].init.fire(sol, items);
          },
          'onOpen': function(sol, changedElements) {
            $wrapper.addClass('sol-open');
            Drupal.settings.views_sol_filter.callbacks[id].open.fire(sol, changedElements);
          },
          'onClose': function(sol, changedElements) {
            $wrapper.removeClass('sol-open');
            Drupal.settings.views_sol_filter.callbacks[id].close.fire(sol, changedElements);
          },
          'onChange': function(sol, changedElements) {
            $.views_sol_filter_configure_sol(sol, true);
            Drupal.settings.views_sol_filter.callbacks[id].change.fire(sol, changedElements);
          }
        };
        var $sol = $el.searchableOptionList(options);
        var $caret = $el.find('.sol-caret-container');

        // $wrapper.children('label')
        //   .addClass('sol-field-label')
        //   .click(function() {
        //     // console.log('foo');
        //     // todo: this toggle doesn't work?!
        //     // $caret.click();
        //     // $el.searchableOptionList().toggle();
        //     $sol.toggle();
        //   });
      });
    }
  };

  $.views_sol_filter_configure_sol = function (sol, displayed, options) {
    var base_name = sol.$originalElement.attr('id').replace('edit-', '');
    var field_name = base_name.replace(/-/g, '_');
    var $wrapper = $('.form-item-' + base_name);
    if (
      Drupal.settings.views_sol_filter.fields.hasOwnProperty(field_name)
      && Drupal.settings.views_sol_filter.fields[field_name].hasOwnProperty('facet')
    ) {
      var $displayed = $('.sol-selected-display-item', $wrapper);
      var $options = $('.sol-option', $wrapper);
      if (options) {
        $.views_sol_filter_hightlight_sol($options, 'sol-label-text', field_name);
      }
      if (displayed) {
        $.views_sol_filter_hightlight_sol($displayed, 'sol-selected-display-item-text', field_name);
      }
    }
  };

  $.views_sol_filter_hightlight_sol = function ($elements, value_class_el, nn_field_name) {
    $.each($elements, function (key, el) {
      // if (!Drupal.settings.views_sol_filter.fields[nn_field_name].hasOwnProperty('facet')) {
      //   return true;
      // }
      var $el = $(el);
      var val = $('.' + value_class_el, $el).html()
        .replace('&lt;', '<')
        .replace('&gt;', '>')
        .replace('&amp;', '&');
      var val_in = Drupal.settings.views_sol_filter.fields[nn_field_name].facet.in.hasOwnProperty(val);
      $el.addClass(val_in ? 'facet-in' : 'facet-out');
      if (Drupal.settings.views_sol_filter.fields[nn_field_name].facet.has.hasOwnProperty(val)) {
        $el.addClass('facet-has');
      }
    });
  };

})(jQuery);